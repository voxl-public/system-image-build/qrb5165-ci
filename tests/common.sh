cyan_underline='\033[4;36m' 
red='\033[0;31m'
green='\033[0;32m'
clear='\033[0m'

echo_test_case(){
	echo -e "${cyan_underline}[TEST] $1${clear}"
}

echo_pass(){
	 echo -e "${green}[PASS] $1${clear}"
}

echo_fail(){
	echo -e "${red}[FAIL] $1${clear}"
}

echo_error(){
	echo -e "${red}[ERROR] $1${clear}"
}