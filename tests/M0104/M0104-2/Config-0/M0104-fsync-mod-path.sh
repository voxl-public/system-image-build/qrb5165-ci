#!/bin/bash

source common.sh

TEST_CASE="M0104-fsync-mod-path"
echo_test_case $TEST_CASE
result=0

paths=(/sys/module/voxl_fsync_mod/parameters)

for path in "${paths[@]}"; do
	if [ ! -e "$path" ]; then
		echo_error "device path missing: $path"
		result=1
	fi
done

if [ $result -eq 0 ]; then
	echo_pass $TEST_CASE
	exit 0
else
	echo_fail $TEST_CASE
	exit 1
    result=1
fi

# Final result
if [ $result -eq 0 ]; then
    echo_pass $TEST_CASE
    exit 0
else
    echo_fail $TEST_CASE
    exit 1
fi
