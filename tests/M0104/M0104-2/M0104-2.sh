#!/bin/bash

# Check the 'machine' parameter
if [ $(cat /sys/module/voxl_platform_mod/parameters/machine) == "2" ]; then
    # Check the 'config' parameter
    if [ $(cat /sys/module/voxl_platform_mod/parameters/config) == "0" ]; then
        ./M0104/M0104-2/Config-0/M0104-2-0.sh
        exit 0
        
    fi
fi

# If none of the conditions are met, exit with error status
exit 1

