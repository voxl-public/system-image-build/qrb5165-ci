#!/bin/bash

source common.sh

TEST_CASE="M0104-fsync-mod-sampling"
echo_test_case $TEST_CASE
result=0

# Define GPIO pins to be tested
gpio_pins=(110 111 112 113 114)

# Define frequencies to test each GPIO pin against
sampling_frequencies=(33333333 900000)

# Define the acceptable tolerance percentage for average difference
tolerance_percentage=2  # Increase to allow greater variance in frequencies compared to sampling

# Define the maximum allowable error percentage for any single spike (difference between two time stamps for signals)
max_time_error=10  # Increase to allow larger spikes in timestamp differences

# Set the number of timestamp samples to collect
num_samples=8  # Increase to improve averaging accuracy, but may require more time

function set_parameter {
    echo $2 > "/sys/module/voxl_fsync_mod/parameters/$1"
}

function get_parameter {
    cat "/sys/module/voxl_fsync_mod/parameters/$1"
}

# Save the original GPIO and sampling period settings to restore later
original_gpio=$(get_parameter "gpio_num")
original_sampling_period=$(get_parameter "sampling_period_ns")
set_parameter "enabled" "1"

function test_gpio_and_frequency {
    local gpio=$1
    local freq=$2
    # Calculate tolerance and max spike allowance based on set percentages
    local tolerance=$(($freq * (100+tolerance_percentage) / 100))
    local max_spike_tolerance=$(($freq * (100+max_time_error) / 100))
    set_parameter "gpio_num" "$gpio"
    set_parameter "sampling_period_ns" "$freq"
    sleep 3

    timestamps=($(timeout 2s head -n $num_samples /dev/voxl-fsync))

    if [ "${#timestamps[@]}" -lt $num_samples ]; then
        echo -e "\e[31mNot enough data collected for GPIO $gpio at frequency $freq\e[0m"
        return 1
    fi

    local last_timestamp="${timestamps[0]}"
    local max_difference=0
    local total_difference=0
    local count=0

    # Calculate total and maximum differences between consecutive timestamps
    for timestamp in "${timestamps[@]:1}"; do
        local diff=$((timestamp - last_timestamp))
        total_difference=$((total_difference + diff))
        if [ "$diff" -gt "$max_difference" ]; then
            max_difference=$diff
        fi
        last_timestamp=$timestamp
        ((count++))
    done

    local avg_difference=$((total_difference / count))
    local avg_error=$((100 * (avg_difference - freq) / freq))

    if [ "$avg_difference" -lt "$tolerance" ] && [ "$max_difference" -lt "$max_spike_tolerance" ]; then
        return 0
    else
        echo -e "\e[31mGPIO $gpio at $freq ns fails tests with avg error $avg_error%: Avg diff $avg_difference ns (Expected < $tolerance ns), Max spike $max_difference ns (Allowed < $max_spike_tolerance ns)\e[0m"
        return 1
    fi
}

# Test all defined GPIO pins at all defined frequencies
for gpio in "${gpio_pins[@]}"; do
    gpio_result=0
    for freq in "${sampling_frequencies[@]}"; do
        if ! test_gpio_and_frequency "$gpio" "$freq"; then
            gpio_result=1
        fi
    done
    if [ "$gpio_result" -eq 0 ]; then
        echo "GPIO $gpio passes all frequency tests."
    else
        echo -e "\e[31mGPIO $gpio fails at least one frequency test.\e[0m"
        result=1
    fi
done

# Restore original settings
set_parameter "gpio_num" "$original_gpio"
set_parameter "sampling_period_ns" "$original_sampling_period"

# Output final result based on testing outcomes
if [ $result -eq 0 ]; then
    echo_pass $TEST_CASE
    exit 0
else
    echo_fail $TEST_CASE
    exit 1
fi

