#!/bin/bash

# include tests to run
./M0104/M0104-1/Config-0/M0104-1-voxl-platform-mod.sh
./M0104/M0104-1/Config-0/M0104-1-cameras.sh

./M0104/M0104-1/Config-0/M0104-uart.sh
./M0104/M0104-1/Config-0/M0104-spi.sh
./M0104/M0104-1/Config-0/M0104-i2c.sh
./M0104/M0104-1/Config-0/M0104-gpio.sh
./M0104/M0104-1/Config-0/M0104-fsync-mod-path.sh
./M0104/M0104-1/Config-0/M0104-fsync-mod-sampling.sh
./M0104/M0104-1/Config-0/M0104-SLPI-UART-Loopback.sh


exit 0
