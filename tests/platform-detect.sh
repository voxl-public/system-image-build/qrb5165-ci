#!/bin/bash

# voxl-platform-mod
if [ $(cat /sys/module/voxl_platform_mod/parameters/machine) == "1" ]; then
	if [ $(cat /sys/module/voxl_platform_mod/parameters/variant) == "0" ]; then
		echo "M0054-1"
		exit 0
	fi
	if [ $(cat /sys/module/voxl_platform_mod/parameters/variant) == "2" ]; then
		echo "M0054-2"
		exit 0
	fi
elif [ $(cat /sys/module/voxl_platform_mod/parameters/machine) == "2" ]; then
	if [ $(cat /sys/module/voxl_platform_mod/parameters/variant) == "0" ]; then
		echo "M0104-1"
		exit 0
	fi
	if [ $(cat /sys/module/voxl_platform_mod/parameters/variant) == "2" ]; then
		echo "M0104-2"
		exit 0
	fi
fi

exit 1