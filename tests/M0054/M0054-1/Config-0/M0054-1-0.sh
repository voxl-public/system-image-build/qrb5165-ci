#!/bin/bash

# include tests to run
./M0054/M0054-1/Config-0/M0054-1-voxl-platform-mod.sh
./M0054/M0054-1/Config-0/M0054-1-cameras.sh

./M0054/M0054-1/Config-0/M0054-uart.sh
./M0054/M0054-1/Config-0/M0054-spi.sh
./M0054/M0054-1/Config-0/M0054-i2c.sh
./M0054/M0054-1/Config-0/M0054-gpio.sh
./M0054/M0054-1/Config-0/M0054-fsync-mod-path.sh
./M0054/M0054-1/Config-0/M0054-fsync-mod-sampling.sh
./M0054/M0054-1/Config-0/M0054-UART-Loopback.sh
./M0054/M0054-1/Config-0/M0054-SLPI-UART-Loopback.sh
exit 0
