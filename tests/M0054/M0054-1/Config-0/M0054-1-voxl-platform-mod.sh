#!/bin/bash

source common.sh

echo_test_case "M0054-1-voxl-platform-mod"

# Define the full paths for the 'machine', 'variant', and 'config' files
machine_file="/sys/module/voxl_platform_mod/parameters/machine"
variant_file="/sys/module/voxl_platform_mod/parameters/variant"
config_file="/sys/module/voxl_platform_mod/parameters/config"

# Initialize variables to hold the content of the machine, variant, and config files
machine_version=""
variant_version=""
config_version=""

# Function to read file content
read_file() {
    local file_path=$1
    if [[ -f "$file_path" ]]; then
        cat "$file_path"
    else
        echo "[ERROR] File not found: $file_path"
        exit 1
    fi
}

# Read the contents of the machine, variant, and config files
machine_version=$(read_file "$machine_file")
variant_version=$(read_file "$variant_file")
config_version=$(read_file "$config_file")

# Check if machine version is 1, variant version is 0, and config version is 0
if [[ "$machine_version" == "1" && "$variant_version" == "0" && "$config_version" == "0" ]]; then
    echo "[INFO] Machine: $machine_version"
    echo "[INFO] Variant: $variant_version"
    echo "[INFO] Config: $config_version"
    echo_pass "voxl-platform-mod"
    exit 0
else
    echo "[INFO] Machine: $machine_version"
    echo "[INFO] Variant: $variant_version"
    echo "[INFO] Config: $config_version"
    echo_fail "voxl-platform-mod"
    exit 1
fi

