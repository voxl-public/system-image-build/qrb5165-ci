#!/bin/bash

source common.sh

TEST_CASE="M0054-gpios"
echo_test_case $TEST_CASE
result=0

# Base path for GPIOs
base_path="/sys/class/gpio"

# Define a fail echo function
echo_fail() {
    echo -e "${red}[FAIL] $1${clear}"
}

# List of expected GPIO folders and their expected states and directions
declare -A expected_gpios=(
    [gpio1100]="out 1"
    [gpio1101]="out 1"
    [gpio1136]="out 0"
    [gpio1137]="out 0"
    [gpio1146]="out 0"
    [gpio1148]="in"
    [gpio1149]="in"
    [gpio1150]="in"
    [gpio1151]="in"
    [gpio1152]="in"
    [gpio1153]="out 1"
    [gpio1154]="out 0"
    [gpio1155]="out 0"
    [gpio1156]="out 1"
    [gpio1157]="out 1"
    [gpio1167]="out 0"
    [gpio1182]="out 0"
    [gpio1183]="out 0"
    [gpio1184]="out 0"
    [gpio1185]="out 1"
    [gpio1186]="out 1"
    [gpio1187]="out 1"
    [gpio1188]="out 1"
    [gpio1189]="out 1"
    [gpio1224]="out 1"
    [gpio1231]="out 0"
    [gpio1252]="out 1"
    [gpio1253]="out 0"
    [gpio1254]="out 0"
    [gpio1255]="out 0"
    [gpio1257]="out 1"
    [gpio1259]="out 1"
)

# Check each expected GPIO folder
for gpio in "${!expected_gpios[@]}"; do
    IFS=' ' read -r -a gpio_props <<< "${expected_gpios[$gpio]}"
    direction=${gpio_props[0]}
    value=${gpio_props[1]:-}  # Default value may not be set for input gpios
    
    full_path="$base_path/$gpio"
    if [ ! -d "$full_path" ]; then
        echo_fail "GPIO folder missing: $full_path"
        result=1
    else
        # Check the direction
        if [ "$(cat "$full_path/direction")" != "$direction" ]; then
            echo_fail "Direction mismatch for $full_path. Expected $direction."
            result=1
        fi
        # If it's an output, also check the value
        if [ "$direction" == "out" ] && [ -n "$value" ]; then
            if [ "$(cat "$full_path/value")" != "$value" ]; then
                echo_fail "Value mismatch for $full_path. Expected $value."
                result=1
            fi
        fi
    fi
done

# Check the overall result and print the final outcome
if [ $result -eq 0 ]; then
    echo_pass $TEST_CASE
    exit 0
else
    echo_fail $TEST_CASE
    exit 1
fi

