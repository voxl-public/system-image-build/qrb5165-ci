#!/bin/bash
echo "Starting script..."

# Check the machine parameter
if [ $(cat /sys/module/voxl_platform_mod/parameters/machine) == "1" ]; then
    
    # Check the config parameter
    if [ $(cat /sys/module/voxl_platform_mod/parameters/config) == "0" ]; then
        echo "Config parameter is 0"
        ./M0054/M0054-1/Config-0/M0054-1-0.sh
        exit 0
    elif [ $(cat /sys/module/voxl_platform_mod/parameters/config) == "1" ]; then
        echo "Config parameter is 1"
        ./M0054/M0054-1/Config-1/M0054-1-1.sh
        exit 0
    fi
fi

echo "Exiting with error..."
exit 1

