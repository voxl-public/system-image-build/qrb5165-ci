#!/bin/bash

source common.sh

TEST_CASE="M0054-UART-Loopback-TEST"
echo_test_case $TEST_CASE
result=0

# List of device identifiers to test
devices=(0 1 2 3)

# Loop through each device identifier
for device in "${devices[@]}"; do
    # Execute the test command for each device and capture the output
    output=$(qrb5165io-uart-test -d $device 2>&1)
    echo "Running qrb5165io-uart-test for device -d $device: $output"

    # Check if the output contains the success message
    if [[ $output != *"[INFO]: success!"* ]]; then
        echo_error "Test failed on device $device: $output"
        result=1
    fi
done

# Check if any tests failed
if [ $result -eq 0 ]; then
    echo_pass $TEST_CASE
    exit 0
else
    echo_fail $TEST_CASE
    exit 1
fi

